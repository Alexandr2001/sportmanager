package ru.vaa.sportmeneger;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Notes extends AppCompatActivity implements View.OnClickListener {

    private EditText editText;
    private Button btnAdd;
    private Button btnClear;
    private SharedPreferences sPref;
    private final String APP_PREFERENCES = "mysettings";
    private ListView notesList;
    private List<String> list = new ArrayList<>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        editText = (EditText) findViewById(R.id.editText);

        btnAdd = (Button) findViewById(R.id.buttonAdd);
        btnAdd.setOnClickListener(this);
        btnClear = (Button) findViewById(R.id.buttonClear);
        btnClear.setOnClickListener(this);

        notesList = (ListView) findViewById(R.id.notesList);
        adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.content, list);
        sPref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        //noinspection ConstantConditions
        list.addAll(sPref.getStringSet(APP_PREFERENCES, new HashSet<String>()));
        notesList.setAdapter(adapter);
        editText.setText(getIntent().getStringExtra("title"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAdd:
                list.add(editText.getText().toString());
                break;
            case R.id.buttonClear:
                list.clear();
                break;
            default:
                break;
        }
        notesList.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences.Editor editor = sPref.edit();
        Set<String> stringSet = new HashSet<>(list);
        editor.putStringSet(APP_PREFERENCES, stringSet);
        editor.apply();
    }
}
